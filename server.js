const express = require('express')
const cors = require('cors')
const healthCheckRoutes = require('./src/routes/healthcheck')
const consumeQueue = require('./consumer');

const app = express()
const port = 3005;

app.use(express.json())
app.use(cors())

app.use('/healthcheck', healthCheckRoutes);

app.get("/", (req, res) => {
})

app.listen(port, () => console.log(`Server is listening on port ${port}`))
